from mcrcon import MCRcon

class MCServer:
    mcr = 0
    def init():
        MCServer.mcr = MCRcon("localhost","minestok")
        MCServer.mcr.connect()
    def command(command_text):
        return MCServer.mcr.command(command_text)
MCServer.init()
print(MCServer.command("/time set day"))
def wstaw_pixel(x, y ,z):
     cmd = "/setblock "+str(x)+" "+str(z)+" "+str(y)+" minecraft:glass"
     print(cmd)
     resp = MCServer.command(cmd)
     print(resp)

def rysuj_okrag(x0,y0,radius,height):
    x = radius - 1
    y = 0
    dx = 1
    dy = 1
    err = dx - (radius << 1)
    while (x >= y):
        wstaw_pixel(x0+x, y0+y, height)
        wstaw_pixel(x0+y, y0+x, height)
        wstaw_pixel(x0-y, y0+x, height)
        wstaw_pixel(x0-x, y0+y, height)
        wstaw_pixel(x0-x, y0-y, height)
        wstaw_pixel(x0-y, y0-x, height)
        wstaw_pixel(x0+y, y0-x, height)
        wstaw_pixel(x0+x, y0-y, height)

        if(err <=0):
            y+=1
            err += dy
            dy += 2
        else:
            x-=1
            dx += 2
            err += dx - (radius << 1)


rysuj_okrag(-260, 95, 50, 90)
